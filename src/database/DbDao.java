package database;

import java.sql.*;

import database.ConnMySql;

/**
 * @author  Weilong Wang ww703@nyu.edu
 * @version  1.0
 */
public class DbDao 
{
	private Connection conn;
	private String driver;
	private String url;
	private String username;
	private String pass;
	private static DbDao _instance=null;
	public DbDao()
	{
		
	}
	
	public static DbDao getInstance()
	{
		if(_instance==null)
		{
			_instance=new DbDao();
		}
		
		return _instance;
	}
	
	
	public DbDao(String driver , String url 
		, String username , String pass)
	{
		this.driver = driver;
		this.url = url;
		this.username = username;
		this.pass = pass; 
	}
	//setters and getters for every attributes
	public void setDriver(String driver) {
		this.driver = driver; 
	}
	public void setUrl(String url) {
		this.url = url; 
	}
	public void setUsername(String username) {
		this.username = username; 
	}
	public void setPass(String pass) {
		this.pass = pass; 
	}
	public String getDriver() {
		return (this.driver); 
	}
	public String getUrl() {
		return (this.url); 
	}
	public String getUsername() {
		return (this.username); 
	}
	public String getPass() {
		return (this.pass); 
	}
	//go to connect database
	public Connection getConnection() throws Exception
	{
		if (conn == null)
		{
			Class.forName(this.driver);
			conn = DriverManager.getConnection(url,username,
				this. pass);
		}
		return conn;
		//return new ConnMySql().getConnection();
	}
	
	//query
	public ResultSet query(String word,String query)
		throws Exception
	{
		 
		 

		PreparedStatement pstmt = getConnection().prepareStatement(query);
		pstmt.setString(1, word);
		
		return pstmt.executeQuery();
	}
	
	//query
		public ResultSet query2(String cate, String subcate, String query)
			throws Exception
		{
			 
			PreparedStatement pstmt = getConnection().prepareStatement(query);
			pstmt.setString(1, cate);
			pstmt.setString(2, subcate);
			
			return pstmt.executeQuery();
		}
	
	
	
	//close database connection
	public void closeConn()
		throws Exception
	{
		if (conn != null && !conn.isClosed())
		{
			conn.close();
		}
	}
}