package ProcessingDocument;
import java.sql.Connection;
import java.sql.ResultSet;
import Helper.descendComparator;
import Parser.ExtractHtmlContent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import database.*;

public class Summary {
	
	public DbDao db=null;
	public int UniversalDocumentNum=15000000;
	public String link;
	public String query;
	public String[] result;
	 
	public Summary(String link, String query)
	{
		this.link=link;
		this.query=query;
		this.result=new String[2];
		try {
			CreatConnectionToDatabase();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String[] getResult( ) throws Exception
	{
 
		 //Help ExtractHtmlContent Class
		 ExtractHtmlContent test=new ExtractHtmlContent(); 
		 
		 //Parse the document and save the sentences in String array
		 ArrayList<String[]> sentenceLists=test.parseParagraph(link);
 
		 //Delete duplicate word and remove punctuation and save word as lowercase. Saving to HashSet
		 HashSet<String> querySet=splitQueryNoDuplicates(query);
	
		 //preCalculate All query word TF of in Query.
		 HashMap<String, Integer> tfQuery=tfQuery(query);

		 //Calculate each word TF of the documet
		 HashMap<String, Integer> dictMap=AllWordFrequenceInDocument(sentenceLists);
		 
		 //preCalculateAllQueryWordTfInDocument
		 HashMap<String, Integer> tfMap=preCalculateAllQueryWordTfInDocument(querySet,dictMap);
		
		 //Find the number of documents in ANC Corpus which including our query word
		 HashMap<String, Integer> containWordOfDocNumberNumMap=containWordOfAllDocNumber(querySet);
					
		 //Calculate All the query word Idf
		 HashMap<String, Double> idfMap=preCalculateAllQueryWordIdfInDocument(querySet, containWordOfDocNumberNumMap);
		 
		 //preCalculateAllQueryWordTfIdfInDocument based on the query TF
		 HashMap<String, Double> AllQueryWordTfIdfInQuery=preCalculateAllQueryWordTfIdfInQuery(querySet, tfQuery, idfMap);
		 
		 //queryNormalize
		 HashMap<String, Double> queryNormalize=queryNormalize(querySet,tfQuery,idfMap);
		 
		 //documentNormalize
		 HashMap<String, Double> documentNormalize=docNormalize(querySet, dictMap);
		// result[0]=queryDocumentSimilarity(querySet,queryNormalize, documentNormalize).toString();
		 System.out.println("The similarity between query and document is "+result[0]);
		 
		 //Collect All sentences which including query words
		 ArrayList<String[]> AllSentenceContainedQWords=collectSentenceContainedQWords(sentenceLists, querySet);
		 String summarization=null;
		 boolean flag=false;
		 for(String word:querySet)
		 {
			 ResultSet rs;
			 String sql="select * from wordcategory where word=?";
			 try {
				 	rs = db.query(word,sql);				
					if(rs.next())
					{
						flag=true;
						break;
					}
					
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		 }
		 
		 if(flag)
		 { //If the query we serached in our three core areas, then go this way
			 //System.out.println("there is this word in database");
			 result[0]=queryDocumentSimilarity(querySet,queryNormalize, documentNormalize).toString();
			 summarization=SummarizationByWeight(sentenceLists,querySet,AllQueryWordTfIdfInQuery);
		 }else
		 {			// System.out.println("there is not this word in database");
			//If the query we serached not in our three core areas, then go this way
			 summarization=SummarizationByCosin(query,tfQuery(query),AllSentenceContainedQWords);
			 result[0]=queryDocumentSimilarity(querySet,queryNormalize, documentNormalize).toString();

		 }
		 result[1]=summarization;
		 return result;
	
}
	
	 public void CreatConnectionToDatabase() throws Exception
	 {
		 db=new DbDao("com.mysql.jdbc.Driver", "jdbc:mysql://127.0.0.1:3306/nlp", "root" , "1234");
		 Connection 	connection=db.getConnection();
		 if (connection != null) 
		 {
				System.out.println("You made it, take control your database now!");
		 } else 
		 {
				System.out.println("Failed to make connection!");
		 }
	 }
	 
	 /**
	  * Delete Duplicates word in Query and remove Punctuation save to HashSet
	  * @param query
	  * @return
	  */
	 public HashSet<String> splitQueryNoDuplicates(String query)
	 {
		 HashSet<String> set=new HashSet<String>();
		 //replace Puncuation with space in the Query
		 String skipPunct=query.replaceAll("[\\p{Punct}]+"," ");
		// System.out.println(skipPunct);
		 //Split the query by any number space
		 String[] arrayQuery=skipPunct.split("\\s{1,}");
		 for(String temp:arrayQuery)
		 {
			 //save query word as LowerCase
			 set.add(temp.toLowerCase());
		 }
		 
		 return set;
	 }
	 
	 /**
	  * Calculate the TF of each word in the Query
	  * @param query
	  * @return
	  */
	 public HashMap<String, Integer> tfQuery(String query)
	 {
		 HashMap<String, Integer> tfQuery=new HashMap<>();
		 String skipPunct=query.replaceAll("[\\p{Punct}]+"," ");//As above
		 String[] arrayQuery=skipPunct.split("\\s{1,}");//As above

		 for(String word:arrayQuery)
		 {
			 word=word.toLowerCase();
			 if(word.trim()!="")
			 {
				 if(!tfQuery.containsKey(word))
				 {
					 tfQuery.put(word, 0);
				 }
				 tfQuery.put(word, tfQuery.get(word)+1); 
			 }
		 }
		 
		 for(String word:arrayQuery)
		 {
			 System.out.println(word.toLowerCase()+"    "+tfQuery.get(word.toLowerCase()));
		 }
		 return tfQuery;		 
	 }
	 
	 /**
	  * preCalculateAllQueryWordTfInDocument
	  * @param arrayQuery
	  * @param dictMap
	  * @return
	  */
	 public HashMap<String, Integer> preCalculateAllQueryWordTfInDocument(HashSet<String> querySet,HashMap<String, Integer> dictMap)
	 {
		 HashMap<String,Integer> tfMap=new HashMap<String, Integer>();
		 
		 for(String word:querySet)
		 {
				tfMap.put(word, preCalculateIndividualQueryWordTfInDocument(dictMap,word));
				System.out.println("The word "+word+" in the document frequence is "+preCalculateIndividualQueryWordTfInDocument(dictMap,word));

		 }
		 return tfMap;
	 }
	 
	 /**
	  * preCalculateIndividualQueryWordTfInDocument
	  * @param map
	  * @param word
	  * @return
	  */
	 public  int preCalculateIndividualQueryWordTfInDocument(HashMap<String, Integer> map, String word)
	 {
		 if(map==null||word==null)
			 return 0;
		 
		 if(map.containsKey(word))
		 {
			 return map.get(word);
		 }
		 return 0;
	 }

	 
	 /**
	  * Calculate each word TF of the documet 
	  * @param lists
	  * @return
	  */
	 public HashMap<String, Integer> AllWordFrequenceInDocument(ArrayList<String[]> lists)
	 {
		 HashMap<String, Integer> map=new HashMap<>();
		 for(String[] list:lists)
		 {
			 for(String word:list)
			 {
				 word=word.toLowerCase();
				 if(word.trim()!="")
				 {
					 if(!map.containsKey(word))
					 {
						 map.put(word, 0);
					 }
					 map.put(word, map.get(word)+1); 
				 }
			 }
		 }
		 return map;
	 }
	 
 
	 public  HashMap<String, Double> tfIdf(String[] arrayQuery, HashMap<String, Double> idfMap, HashMap<String, Integer> tfMap )
	 {
		 HashMap<String, Double> tfIdf=new HashMap<>();
		 for(String word:arrayQuery)
		 {
			 Double tfValue=Double.valueOf(tfMap.get(word));
			 Double idfValue=idfMap.get(word);
			 tfIdf.put(word, tfValue*idfValue);
		 }
		 return tfIdf;
	 }
	 
	
	 /**
	  * Find the number of documents in ANC Corpus which including our query word
	  * @param querySet
	  * @return
	  */
	 public HashMap<String, Integer> containWordOfAllDocNumber(HashSet<String> querySet)
	 {
			HashMap<String, Integer> containWordOfAllDocNumberMap=new HashMap<String, Integer>();
			ResultSet rs;
			String sql="select Freq from NLP where word=?";
			for(String word:querySet)
			{
					try {
							rs = db.query(word,sql);
							 if(rs.next())
							 {
								 containWordOfAllDocNumberMap.put(word, rs.getInt(1));
								 System.out.println("In ANC the number of Documents contains "+word+" is "+rs.getInt(1));
							 }else
							 {
								 containWordOfAllDocNumberMap.put(word, 0);
								 System.out.println("In ANC the number of Documents contains "+word+" is 0");
							 }
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			return containWordOfAllDocNumberMap;	
		}
	
	/**
	 * Calculate All the query word Idf
	 * @param querySet
	 * @param containWordOfDocNumberNumMap
	 * @return
	 */
	public HashMap<String, Double> preCalculateAllQueryWordIdfInDocument(HashSet<String> querySet,HashMap<String, Integer> containWordOfDocNumberNumMap)
	{
		HashMap<String, Double> idfMap=new HashMap<String, Double>();
		for(String word:querySet)
		{
			
			Double idf=preCalculateIdf(word,containWordOfDocNumberNumMap);
			System.out.println("The word "+word+" in the query idf is "+idf);
			idfMap.put(word, idf);
		}
		
		return idfMap;
	}

	
	/**
	 * calculate individual query word Idf
	 * @param word
	 * @param containWordOfDocNumberNumMap
	 * @return
	 */
		public Double preCalculateIdf(String word,HashMap<String, Integer> containWordOfDocNumberNumMap)
		{
			Double idf=Math.log10(UniversalDocumentNum/(containWordOfDocNumberNumMap.get(word)+1));
			return idf;
		}
	
	/**
	 * 
	 * @param querySet
	 * @param queryNormalize
	 * @param documentNormalize
	 * @return
	 */
	public Double queryDocumentSimilarity(HashSet<String> querySet,HashMap<String, Double> queryNormalize, HashMap<String, Double> documentNormalize)
	{
		Double similarity=0.00;
		for(String word: querySet)
		{
			Double normalizeQ=queryNormalize.get(word);
			Double normalizeD=documentNormalize.get(word);
			//System.out.println(word+" similarity is "+normalizeQ+"   "+normalizeD);
			similarity+=normalizeQ*normalizeD;
			
		}
		return similarity;
	}
	
	/**
	 * 
	 * @param querySet
	 * @param tfQuery
	 * @param idfMap
	 * @return
	 */
	public HashMap<String, Double> queryNormalize(HashSet<String> querySet, HashMap<String, Integer>tfQuery,HashMap<String, Double>idfMap)
	{
		HashMap<String, Double> queryNormalize=new HashMap<String, Double>();
		HashMap<String, Double> querywt=new HashMap<String, Double>();

		Double queryLength=0.0;
		for(String word:querySet)
		{
			Double idf=idfMap.get(word);
			Double tfwt=1+Math.log10(tfQuery.get(word));
			System.out.println("In the Query "+word+" tfwt is "+tfwt);
			System.out.println("In the Query "+word+" idf is "+idf);

			Double wt=tfwt*idf;
			System.out.println("In the Query "+word+" wt is "+idf);

			querywt.put(word, wt);
			queryLength+=wt*wt;
		}
		System.out.println("queryLength is "+queryLength);

		queryLength=Math.sqrt(queryLength);
		System.out.println("After sqr queryLength is "+queryLength);
		for(String word:querySet)
		{
			queryNormalize.put(word, querywt.get(word)/queryLength);
			System.out.println("In the Query "+word+" normalize is "+querywt.get(word)/queryLength);

		}
		return queryNormalize;
	}
	
	/**
	 * 
	 * @param querySet
	 * @param dictMap
	 * @return
	 */
	public HashMap<String, Double> docNormalize(HashSet<String> querySet, HashMap<String, Integer> dictMap)
	{
		HashMap<String, Double> docNormalize=new HashMap<String, Double>();
		HashMap<String, Double> docwt=new HashMap<String, Double>();
		
		Double docLength=0.0;

		for(String word:querySet)
		{
			Double wt=0.0;
			if(dictMap.get(word)!=null)
			{
				//System.out.println("Document 里面 "+word+" 的频率是 "+dictMap.get(word));
				wt=1+Math.log10(dictMap.get(word)+1);
			}

			docwt.put(word, wt);
			System.out.println("In the Document "+word+" wt is "+docwt.get(word));

			docLength+=wt*wt;
		}
		System.out.println("Document length is "+docLength);

		docLength=Math.sqrt(docLength);
		System.out.println("After sqr document length is "+docLength);

		for(String word:querySet)
		{
			docNormalize.put(word, docwt.get(word)/docLength);
			System.out.println("In the Document "+word+" normalize is "+docwt.get(word)/docLength);

		}
		return docNormalize;
	}
	
	/**
	 * Collect All sentences which including query words
	 * @param lists
	 * @param arrayQuery
	 * @return
	 */
	public  ArrayList<String[]> collectSentenceContainedQWords(ArrayList<String[]>lists, HashSet<String> arrayQuery)
	{
		 ArrayList<String[]> sentencesCollection=new ArrayList<>();
		 for(String[] list:lists)
		 {
			 for(String word:list)
			 {
				 word=word.toLowerCase();
				 if(word.trim()!="")
				 {
					 if(arrayQuery.contains(word))
					 {
						 sentencesCollection.add(list);
						 for(String a:list)
						 {
							 System.out.print(a+" ");
						 }
						 System.out.println();
						 break;
					 }
					 
				 }
			 }
			 
		 }
		return sentencesCollection;
	}
	
	/**
	 * 
	 * @param query
	 * @param tfQuery
	 * @param lists
	 */
	public String SummarizationByCosin(String query, HashMap<String, Integer> tfQuery, ArrayList<String[]> lists )
	{
		TreeMap<Double, String> sentencesCosin=new TreeMap<Double, String>(new descendComparator());  
		          
		for(String[] sentence:lists)
		{
			StringBuilder sb=new StringBuilder();
			for(String str:sentence)
			{
				if(str.equals(""))
					continue;
				sb.append(str).append(" ");
			}
			sentencesCosin.put(QueryAndSentenceSimilarity(query,sentence, tfQuery),sb.toString());
		}
		
		Iterator it=sentencesCosin.entrySet().iterator();
		while(it.hasNext())
		{
			Map.Entry entry=(Map.Entry)it.next();
			Object key=entry.getKey();
			Object val=entry.getValue();
			System.out.println(key+" :"+val);
		}
		
		StringBuilder sb=new StringBuilder();

		Iterator itt=sentencesCosin.entrySet().iterator();
		for(int i=0; i<4; i++)
		{
			if(itt.hasNext())
			{
				Map.Entry entry=(Map.Entry)itt.next();
				//Object key=entry.getKey();
				Object val=entry.getValue();
				//System.out.println(key+" : "+val);
				if(val.equals(""))
					continue;
				sb.append(val.toString().trim()).append(". ");
			}
		}
		
		System.out.println(sb.toString());
		return sb.toString();

	}
	
	/**
	 * 
	 * @param query
	 * @param sentence
	 * @param tfQuery
	 * @return
	 */
	public double QueryAndSentenceSimilarity(String query, String[] sentence, HashMap<String, Integer> tfQuery)
	{
		
			StringBuilder strb=new StringBuilder();
			for(String str:sentence)
			{
				strb.append(str.toLowerCase()).append(" ");
			}
			String[] newSentence=strb.toString().replaceAll("[\\p{Punct}]+"," ").split("\\s{1,}");
			String[] newquery=query.toLowerCase().replaceAll("[\\p{Punct}]+"," ").split("\\s{1,}");
			
			HashSet<String> mergeSet=new HashSet<String>();
			
			for(String str:newSentence)
			{
				if(str.equals(""))
					continue;
				mergeSet.add(str);
			}
			for(String str:newquery)
			{
				mergeSet.add(str);
			}
			HashMap<String, Integer> tfSentence=tfSentence(newSentence);
		
			ArrayList<Integer> querylist=new ArrayList<>();
			ArrayList<Integer> sentencelist=new ArrayList<>();
			for(String str:mergeSet)
			{
				System.out.print(str+" ");
				if(tfQuery.get(str)!=null)
				{
					querylist.add(tfQuery.get(str));
				}else
				{
					querylist.add(0);
				}
				
				if(tfSentence.get(str)!=null)
				{
					sentencelist.add(tfSentence.get(str));
				}else
				{
					sentencelist.add(0);
				}	
			}
				
			int querysumofsquares=0;
			int sentencesumofsquares=0;
			int multiplysum=0;
			for(int i=0; i<mergeSet.size(); i++)
			{
				multiplysum+=querylist.get(i)*sentencelist.get(i);
				querysumofsquares+=querylist.get(i)*querylist.get(i);
				
				sentencesumofsquares+=sentencelist.get(i)*sentencelist.get(i);
			}
			double cosin=multiplysum/(Math.sqrt(querysumofsquares)*Math.sqrt(sentencesumofsquares));
			//System.out.println("The cosin is "+cosin);
			return cosin;
		}
	
	
	/**
	 * Calculate word Frequence of each Sentence in the document
	 * @param sentence
	 * @return
	 */
	public HashMap<String, Integer> tfSentence(String[] sentence)
	{
		 HashMap<String, Integer> tfSentence=new HashMap<>();
		 for(String word:sentence)
		 {
			 word=word.toLowerCase();
			 if(word.trim()!="")
			 {
				 if(!tfSentence.containsKey(word))
				 {
					 tfSentence.put(word, 0);
				 }
				 tfSentence.put(word, tfSentence.get(word)+1); 
			 }
		 }
		 
		 for(String word:sentence)
		 {
			 System.out.println(word+" int the sentence frequence is "+tfSentence.get(word.toLowerCase()));
		 }
		 return tfSentence; 
	 }
		
	/**
	  * 
	  * @param queryset
	  * @return
	  */
	 public classify classifySearchQuery(HashSet<String> queryset)
	 {
		 HashMap<String, Integer> catclass=new HashMap<String, Integer>();
		 HashMap<String, Integer> subclass=new HashMap<String, Integer>();

		 ResultSet rs;
		 String query="select cate from wordcategory where word=?";
		 
		 String cate=null;
		 String subcate=null;
		 for(String word: queryset)
		 {
			 System.out.print(word);
			 try {
					rs = db.query(word,query);
					
					 if(rs.next())
					 {
						 cate=rs.getString(1);
						 if(catclass.containsKey(cate))
						 {
							 catclass.put(cate, catclass.get(cate)+1);
						 }else
						 {
							 catclass.put(cate, 1);
						 }
						 System.out.println(word+" cate is "+cate);
					 }else
					 {
						 System.out.println("no this kind cate word");
					 }
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String query2="select subcate from wordcategory where word=?";
					 try {
							rs = db.query(word,query2);
							
							 if(rs.next())
							 {
								 subcate=rs.getString(1);
								 if(subclass.containsKey(subcate))
								 {
									 subclass.put(subcate, subclass.get(subcate)+1);
								 }else
								 {
									 subclass.put(subcate, 1);
								 }
								 System.out.println(word+" subcate is "+subcate);
							 }else
							 {
								 System.out.println("no this kind subcatword");
							 }
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		 }
		System.out.println("subclass set size is "+subclass.size());
		System.out.println("catclass set size is "+catclass.size());

	     String maxcate = null;
	     String maxsubcate=null;
	     ArrayList<Integer>list=new ArrayList<>();
	     
	     Iterator ite=catclass.entrySet().iterator();
	     while(ite.hasNext())
	     {
	           Map.Entry entry =(Map.Entry)ite.next();
	           Integer value = (Integer) entry.getValue();
	           list.add(value);
	           Collections.sort(list);
	     } 
	     Iterator ite2=catclass.entrySet().iterator();
	     while(ite2.hasNext())
	     {
	           Map.Entry entry =(Map.Entry)ite2.next();
	           Integer value = (Integer) entry.getValue();
	           if(value == Integer.parseInt(list.get(list.size()-1).toString()))
	           {
	        	   	maxcate = entry.getKey().toString();
	           }
	     } 
	     list.clear();
	     Iterator ite3=subclass.entrySet().iterator();
	     while(ite3.hasNext())
	     {
;	           Map.Entry entry =(Map.Entry)ite3.next();
	           Integer value = (Integer) entry.getValue();
	           list.add(value);
	           Collections.sort(list);
	     } 
	     Iterator ite4=subclass.entrySet().iterator();
	     while(ite4.hasNext())
	     {
	           Map.Entry entry =(Map.Entry)ite4.next();
	           Integer value = (Integer) entry.getValue();
	           if(value == Integer.parseInt(list.get(list.size()-1).toString()))
	           {
	        	   	maxsubcate = entry.getKey().toString();
	           }
	     } 
	         
		 System.out.println(maxcate+" "+catclass.get(maxcate)+"  "+maxsubcate+" "+subclass.get(maxsubcate));
		 return new classify(maxcate,maxsubcate);
	 }
	 
	 
	 /**
	  * 
	  * @param lists
	  * @param querySet
	  * @param AllQueryWordTfIdfInQuery
	  */
	 public String SummarizationByWeight(ArrayList<String[]> lists,HashSet<String> querySet,HashMap<String, Double> AllQueryWordTfIdfInQuery)
	 {
		 classify queryclass=classifySearchQuery(querySet);
		 getAllwordaboutQueryClass(queryclass);
		 
		 HashMap<String[], Double> map=getHighWeightedSentence(lists, querySet, queryclass, AllQueryWordTfIdfInQuery);
		 
		 TreeMap<Double, String[]> treemap=new TreeMap<Double, String[]>(new descendComparator());  
        
		 Iterator it=map.entrySet().iterator();
		 while(it.hasNext())
		 {
			 	Map.Entry entry=(Map.Entry)it.next();
				String[] key=(String[]) entry.getKey();
				Double val=(Double) entry.getValue();
				treemap.put(val, key);
		 }
		 ArrayList<String[]> list=new ArrayList<String[]>();
		 Iterator itt=treemap.entrySet().iterator();
		 int total=0;
		 StringBuilder sb=new StringBuilder();
		 HashSet<String> setset=new HashSet<>();
		 if(treemap.size()>=4)
		 {
			 while(itt.hasNext())
			 {
				 	Map.Entry entry=(Map.Entry)itt.next();
					String[] val=(String[])entry.getValue();
					
					Double key=(Double) entry.getKey();
					list.add(val);
					for(String str:val)
					{
						if(str.equals(""))
							continue;
						sb.append(str).append(" ");
					}
					sb.append(". ");
					total++;
					if(total>4)
						break;

			 }
		 }else
		 {
			 while(itt.hasNext())
			 {
				 	Map.Entry entry=(Map.Entry)itt.next();
					String[] val=(String[])entry.getValue();
					
					Double key=(Double) entry.getKey();
					list.add(val);
					for(String str:val)
					{
						if(str.equals(""))
							continue;
						sb.append(str).append(" ");
					}
					sb.append(". ");
					total++;

			 }
		 }
		//System.out.println(sb.toString());
		 return sb.toString();
		
	 }
	 
	 /**
	  * 
	  * @param queryclass
	  * @return
	  */
	 public ArrayList<String>  getAllwordaboutQueryClass(classify queryclass)
	 {
		 ArrayList<String> wordlist=new ArrayList<String>();
		 String query="select word from wordcategory where cate=? and subcate=?";
		 String cate=queryclass.getcate();
		 String subcate=queryclass.getsubcate();
		 ResultSet rs;

			 try {
					rs = db.query2(cate,subcate,query);
					
					 while(rs.next())
					 {
						 System.out.println(rs.getString(1));
						 wordlist.add(rs.getString(1));
						 
						 
					 }
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			 return wordlist;
	 }
	 
	 /**
	  * 
	  * @param lists
	  * @param querrySet
	  * @param queryclass
	  * @param AllQueryWordTfIdfInQuery
	  * @return
	  */
	 public HashMap<String[], Double>  getHighWeightedSentence(ArrayList<String[]> lists,HashSet<String> querrySet, classify queryclass,HashMap<String, Double> AllQueryWordTfIdfInQuery)
	 {
		 HashMap<String[], Double> weightedSentence=new HashMap<>();
		 for(String[] sentence:lists)
		 {
			 double weightscore=0.0;
			 for(String word: sentence)
			 {
				 if(querrySet.contains(word))
				 {
					 //System.out.println("fuckcicicici");
					  weightscore+=AllQueryWordTfIdfInQuery.get(word);
				 }else if(belongstocate(word, queryclass.getcate())&&belongstosubcate(word, queryclass.getsubcate()))
				 {
					 weightscore+=5.00;
				 }else if(belongstocate(word, queryclass.getcate()))
				{
					 weightscore+=0.25;
				}
			 }
			 weightedSentence.put(sentence, weightscore);
			// System.out.println(weightscore);
			// System.out.println(String.valueOf(sentence));
		 }
		 return weightedSentence;
	 }
	 
	 /**
	  * 
	  * @param word
	  * @param cate
	  * @return
	  */
	 public boolean belongstocate(String word, String cate )
	 {
		 boolean flag = false;
		 ResultSet rs;
		 String localcate;
		 String sql="select cate from wordcategory where word=?";
		 try {
			 	rs = db.query(word,sql);				
				 if(rs.next())
				 {
					 localcate=rs.getString(1);
					 if(localcate.equals(cate))
					 {
						 flag=true;
					 }
				 }else
				 {
					 return  flag;
				 }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 return flag;
	 }
	 
	 /**
	  * 
	  * @param word
	  * @param cate
	  * @return
	  */
	 public boolean belongstosubcate(String word, String cate)
	 {
		 boolean flag = false;
		 ResultSet rs;
		 String localcate;
		 String sql="select subcate from wordcategory where word=?";
		 try {
			 	rs = db.query(word,sql);				
				 if(rs.next())
				 {
					 localcate=rs.getString(1);
					 if(localcate.equals(cate))
					 {
						 flag=true;
					 }
				 }else
				 {
					 return  flag;
				 }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 return flag;
	 }
	 
	 /**
		 * 
		 * @param querySet
		 * @param tfMap
		 * @param idfMap
		 * @return
		 */
		public static HashMap<String, Double> preCalculateAllQueryWordTfIdfInQuery(HashSet<String> querySet, HashMap<String, Integer> tfQuery, HashMap<String, Double>idfMap)
		{
			 	HashMap<String, Double> tfIdfVector=new HashMap<>();
				for(String word:querySet)
				{
					 Double tfValue=Double.valueOf(tfQuery.get(word));
					 Double idfValue=idfMap.get(word );
					 tfIdfVector.put(word,tfValue*idfValue);
					 System.out.println("The word "+word+" based on Query TF-IDF is "+tfValue*idfValue);
				}
				return tfIdfVector;
		}
		
	
}

