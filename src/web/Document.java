package web;

public class Document {
	
	private String title;
	private String link;
	private String displayLink;
	private String snippet;
	private String htmlsnippet;
	
	public Document(String title, String link, String displayLink, String snippet, String htmlsnippet)
	{
		this.title=title;
		this.link=link;
		this.displayLink=displayLink;
		this.htmlsnippet=htmlsnippet;
		this.snippet=snippet;
	}

	public String getSnippet() {
		return snippet;
	}

	public void setSnippet(String snippet) {
		this.snippet = snippet;
	}

	public String getHtmlsnippet() {
		return htmlsnippet;
	}

	public void setHtmlsnippet(String htmlsnippet) {
		this.htmlsnippet = htmlsnippet;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDisplayLink() {
		return displayLink;
	}

	public void setDisplayLink(String displayLink) {
		this.displayLink = displayLink;
	}
	
	

}
