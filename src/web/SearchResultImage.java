package web;

public class SearchResultImage {
//  "contextLink": "http://zh.wikipedia.org/wiki/%E8%8A%B1",
//  "height": 188,
//  "width": 250,
//  "byteSize": 24465,
//  "thumbnailLink": "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQU2J6pZj2kHhl39Z5bjndI1RJXgb7Jbx1xLV7kc2crbt9VbFoJiUDJlQ",
//  "thumbnailHeight": 83,
//  "thumbnailWidth": 111
	public String contextLink;
	public int height;
	public int width;
	public int byteSize;
	public String thumbnailLink;
	public int thumbnailHeight;
	public int thumbnailWidth;
}
