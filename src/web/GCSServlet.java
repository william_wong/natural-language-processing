package web;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Thread.summaryThread;

import com.google.gson.Gson;



public class GCSServlet extends HttpServlet {
	private static final long serialVersionUID = -1275024561023110262L;
	private static final int RESULT_OK = 200;
	private static final int BUF_SIZE = 1024 * 8; // 8k
	private static final String API_KEY = "AIzaSyB5ACXh8dHiuqP_xPzQnviXZZySLGrlF4c"; // API
																						// key
	private static final String UNIQUE_ID = "012840940082329452948:wrc69zmrkja"; // Unique
																					// ID

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		handle(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		handle(request, response);
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 * @throws ServletException 
	 */
	private void handle(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		

		int start = Integer.parseInt(request.getParameter("start"));
		String queryExpression= request.getParameter("queryexpression");
		System.out.println(queryExpression);
		String[] queryExpression2=queryExpression.split(" ");
		StringBuilder sb=new StringBuilder();
		for(int i=0; i<queryExpression2.length-1; i++)
		{
			sb.append(queryExpression2[i]).append("+");
		}
		sb.append(queryExpression2[queryExpression2.length-1]);
		System.out.println(sb.toString());
		
		
		
		String strRequest = "https://www.googleapis.com/customsearch/v1?key=%API_KEY%&cx=%UNIQUE_ID%&q=%queryExpression%&start=%start%";
		strRequest = strRequest.replace("%API_KEY%", API_KEY)
				.replace("%UNIQUE_ID%", UNIQUE_ID)
				.replace("%queryExpression%", sb.toString())
				.replace("%start%", String.valueOf(start));
        System.out.println(strRequest);
		HttpURLConnection conn = null;
		String queryResult = null;
		try {
			URL url = new URL(strRequest);
			conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			

			int resultCode = conn.getResponseCode();
			System.out.println(resultCode);

			if (resultCode == RESULT_OK) {
				InputStream is = conn.getInputStream();
				
				queryResult = readAsString(is);
				System.out.println(queryResult);
				is.close();
			} else {

				System.out.println("Fault on getting http result, code: " + resultCode);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			conn.disconnect();
		}

		if ((queryResult != null) && (queryResult.length() != 0)) {
			
			String respStr = parseResult(queryResult,queryExpression);
			response.setContentType("application/json; charset=utf-8");
			response.setHeader("pragma", "no-cache");
			response.setHeader("cache-control", "no-cache");
			PrintWriter writer = response.getWriter();
			writer.write(respStr);
			writer.flush();
			writer.close(); 

		}
	}

	
	private String parseResult(String queryResult,String query) {
		 	Gson gson = new Gson();  
	        SearchResult e = gson.fromJson(queryResult, SearchResult.class);  
	        System.out.println(queryResult);  
	        SearchResultItem[] items = e.getItems();  
	        Document documents[]=new Document[items.length];
	        ArrayList<summaryThread> list=new ArrayList<summaryThread>();
	        for (int i = 0; i < items.length; i++) 
	        { 
	       	 	summaryThread thread=new summaryThread(query, documents, i,items);
	       	 	thread.start();
	       	 	list.add(thread);	       	 	
	        }  
	        try
	        {
	        		for(summaryThread thread:list)
	        		{
	        			thread.join();
	        		}
	        }catch(InterruptedException ee)
	        {
	        		ee.printStackTrace();
	        }
	     
	        return gson.toJson(documents);
	}

	
	public static String readAsString(InputStream ins) throws IOException {
		ByteArrayOutputStream outs = new ByteArrayOutputStream();
		byte[] buffer = new byte[BUF_SIZE];
		int len = -1;
		try {
			while ((len = ins.read(buffer)) != -1) {
				outs.write(buffer, 0, len);
			}
		} finally {
			outs.flush();
			outs.close();
		}
		return outs.toString();
	}

}

