package Helper;

import java.util.Comparator;

public class descendComparator implements Comparator
{
    public int compare(Object o1,Object o2)
    {
        Double i1=(Double)o1;
        Double i2=(Double)o2;
        return -i1.compareTo(i2);
    }
}
