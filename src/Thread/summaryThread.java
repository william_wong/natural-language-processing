package Thread;
import ProcessingDocument.Summary;
import web.Document;
import web.SearchResultItem;

public class summaryThread extends Thread{

	
	public String query;
	private Document[] documents;
	public int i;
	public SearchResultItem[] items; 

	public summaryThread(String query, Document[] documents,int i, SearchResultItem[]items)
	{
		
		this.query=query;
		this.documents=documents;
		this.i=i;
		this.items=items;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			String link=items[i].link;
			String[] result=new Summary(link,query).getResult();
			String htmlSnippet=result[1];
			String documentSimilarity=result[0];
			char[]  title=items[i].title.trim().toCharArray();
			String title2=String.valueOf(title);

			String title3=title2+"("+documentSimilarity+")";
			documents[i] = new Document(title3, items[i].link, items[i].displayLink, htmlSnippet, items[i].snippet);  

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
