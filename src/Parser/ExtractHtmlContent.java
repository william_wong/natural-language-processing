package Parser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.*;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ExtractHtmlContent {
	
	public ArrayList<String[]> parseParagraph(String url)
	{
		ArrayList<String[]> list=new ArrayList<String[]>();
		
		try {
			org.jsoup.nodes.Document doc=Jsoup.connect(url).get();
			Elements pTags=doc.getElementsByTag("p");
			StringBuilder strb=new StringBuilder();
			for(Element ptag:pTags)
			{
				String str=ptag.text();
				strb.append(str);
			}
			
			/*for(int i=1; i<=6; i++)
			{
				String hTagStr="h"+i;
				Elements hTags=doc.getElementsByTag(hTagStr);
				if(hTags!=null)
				{
					for(Element hTag: hTags)
					{
						String str=hTag.text();
						list.add(str.split(" "));
					}
				}
			}
			*/
			//clean any other character in front of sentence
			for(String temp:getSentences(strb.toString()))
			{
				System.out.println(temp.trim());
				char[] temparray=temp.trim().toCharArray();
				int i=0;
				for(; i<temparray.length;i++)
				{
					if((97<=temparray[i]&&temparray[i]<=122)||(65<=temparray[i]&&temparray[i]<=90))
					{
						break;
					}
				}
				char[] finalarray= Arrays.copyOfRange(temparray, i, temparray.length);
				System.out.println(String.valueOf(finalarray));
				list.add(String.valueOf(finalarray).split(" "));
				
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
	
	public String[] getSentences(String paragraph)
	{
		if(paragraph!=null)
		{
			return paragraph.split("(\\.|!|\\?)");
		}else
		{
			return null;
		}
	}
	
	public static String ReString(String str)
	{
		String dest="";
		if(str!=null)
		{
			Pattern p=Pattern.compile("\n");
			Matcher m=p.matcher(str);
			dest=m.replaceAll("");			
		}
		return dest;
	}

}
